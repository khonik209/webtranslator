class Webtrans {
    constructor(el, settings) {
        this.el = el;
        this.settings = settings;

        this.textArray = []; // Array of a texts
        this.textElements = []; // All elements with text
        this.key = settings.key; //1

// Найдено
        this.translatedText = [];
        this.currentLang = {}; // Selected lang
        this.allLangs = []; // All available langs
        this.baseLang = {};

        this.head = document.getElementsByTagName('head')[0];
        this.link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = 'http://webtrans.local/storage/client/translate.css';
        link.media = 'all';


        if (document.createStyleSheet) {
            document.createStyleSheet('http://webtrans.local/storage/client/translate.css');
        } else {
            head.append(link);
        }

        this.initLibrary();
    }

    static inIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }


    /**
     * Main Logic
     */

    static initLibrary() {
        this.findChild($("body")); // Нашли весь текст
        if (this.inIframe()) {
            parent.postMessage({data: this.textArray}, "http://webtrans.local/");
        }
        this.askServer(); // Запросили у сервера перевод
    }

    /**
     * FIND ALL TEXT
     *
     * @param elem
     */

    findChild(elem) {
        var cText = elem.clone()    //clone the element
            .children() //select all the children
            .remove()   //remove all the children
            .end()  //again go back to selected element
            .text().replace(/\n/g, '').trim();

        if (cText.length > 0) {
            this.textArray.push(cText);
            this.textElements.push(elem);
            // console.log(elem);
            if (this.inIframe()) {
                elem.mouseover(function (e) {
                    elem.addClass("translate-over")
                });
                elem.mouseout(function () {
                    elem.removeClass("translate-over")
                });
                elem.click(function () {
                    parent.postMessage({index: cText}, "http://webtrans.local/");

                    if (elem.prop("tagName") == "BUTTON" || elem.prop("tagName") == "A") {
                        return confirm("Open link?");
                    }
                    return false;
                })
            }
        }


        if (elem.children().length > 0) {
            let self = this;
            elem.children().map(function () {
                self.findChild($(this))
            })
        }
    }

    static askServer() {
        $.ajax({
            type: 'POST',
            data: {
                key: this.key,
                url: window.location.href,
                data: this.textArray
            },
            url: 'http://webtrans.local/api/translate',
            crossDomain: true,
            success: this.ajaxSuccess
        });
    }


    ajaxSuccess(msg) {
        this.translatedText = msg.translation; // получили файлик с переводом с долбанули его в массив
        this.currentLang = msg.baseLanguage; // выбран исходный язык
        this.baseLang = msg.baseLanguage;
        this.allLangs = msg.languages;


        let browserLang = navigator.language || navigator.userLanguage;

        browserLang = browserLang.split("-")[0];

        let self = this;

        let cookieLang = this.allLangs.filter(function (e) {
            return e.code === self.getCookie("current_language");
        })[0];

        if (cookieLang) {
            this.currentLang = cookieLang;
        } else {
            let blang = this.allLangs.filter(function (e) {
                return e.code === browserLang
            })[0];
            if (blang) {
                this.currentLang = blang;
            }
        }
        this.updateLang(this.currentLang);
        this.addSwitchLangBtn(msg.selector);

        if (this.inIframe()) {
            parent.postMessage({loaded: true}, "http://webtrans.local/");
        }
    }

// возвращает cookie с именем name, если есть, если нет, то undefined
    static getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    /**
     * ADD A BUTTON IN SPAN
     *
     * @returns {number}
     */

    addSwitchLangBtn(selector) {
        var place = $("#"+this.el);
        if (place.length === 0) {
            $("body").prepend("<div id='"+ this.el +"' class='swlgbtn-default'></div>");
            place = $("#"+this.el);
        }

        if (place.children().length > 0) {
            place.empty()
        }

        // Render block

        var btnOpenCode = "<div id=\"selector-container\">" +
            "  <ul class=\"selector-list\">";
        var btnCloseCode =
            "  </ul>" +
            " <button class=\"selector-btn\" style=''>" +
            this.currentLang.name +
            "</button>" +
            "</div>";

        var responseCode = btnOpenCode;
        for (var i = 0; i < this.allLangs.length; i++) {
            responseCode = responseCode + "<a class=\"selector-list-item\" data-lang='" + this.allLangs[i].id + "' href=\"#\">" + this.allLangs[i].name + "</a>"
        }
        responseCode = responseCode + btnCloseCode;

        place.append(responseCode);

        var selectorStyleObj = JSON.parse(selector);
        // кнопка
        for (var btnKey in selectorStyleObj["btn"]) {
            if (btnKey === "hover") {
                $(".selector-btn").hover(function () {
                    $(this).css({"background": selectorStyleObj["btn"]["hover"]});
                }, function () {
                    $(this).css({"background": selectorStyleObj["btn"]["background"]})
                });
            } else {
                var cssBtn = {};
                if (btnKey === 'font-size' || btnKey === 'padding-left' || btnKey === 'padding-top' || btnKey === 'padding-right' || btnKey === 'padding-bottom') {
                    cssBtn[btnKey] = selectorStyleObj["btn"][btnKey] + "px";
                } else {
                    cssBtn[btnKey] = selectorStyleObj["btn"][btnKey];
                }
                $(".selector-btn").css(cssBtn);
            }
        }
        // список
        for (var listKey in selectorStyleObj["list"]) {
            if (listKey === "hover") {
                $(".selector-list").hover(function () {
                    $(this).css({"background": selectorStyleObj["list"]["hover"]});
                }, function () {
                    $(this).css({"background": selectorStyleObj["list"]["background"]})
                });
            } else {
                var cssList = {};
                if (listKey === 'font-size' || listKey === 'padding-left' || listKey === 'padding-top' || listKey === 'padding-right' || listKey === 'padding-bottom') {
                    cssList[listKey] = selectorStyleObj["list"][listKey] + "px";
                } else {
                    cssList[listKey] = selectorStyleObj["list"][listKey];
                }
                $(".selector-list").css(cssList);
            }
        }
        // ссылка
        for (var listItemKey in selectorStyleObj["list-item"]) {
            if (listItemKey === "hover") {
                $(".selector-list-item").hover(function () {
                    $(this).css({"background": selectorStyleObj["list-item"]["hover"]});
                }, function () {
                    $(this).css({"background": selectorStyleObj["list-item"]["background"]})
                });
            } else {
                var cssListItem = {};
                if (listItemKey === 'font-size' || listItemKey === 'padding-left' || listItemKey === 'padding-top' || listItemKey === 'padding-right' || listItemKey === 'padding-bottom') {
                    cssListItem[listItemKey] = selectorStyleObj["list-item"][listItemKey] + "px";
                } else {
                    cssListItem[listItemKey] = selectorStyleObj["list-item"][listItemKey];
                }
                $(".selector-list-item").css(cssListItem);
            }
        }

        // add events

        var selectorList = $(".selector-list");
        $('.selector-btn').click(function () {
            if (selectorList.css("display") === 'block') {
                $(".selector-list").css({"display": "none"})

            } else {
                $(".selector-list").css({"display": "block"})
            }
        });

        let self = this;

        $(".selector-list-item").click(function () {
            var needLangId = $(this).data("lang");

            this.currentLang = this.allLangs.filter(function (e, i) {

                return e.id === needLangId
            })[0];

            document.cookie = "current_language=" + this.currentLang.code;

            self.addSwitchLangBtn(selector);
            self.updateLang(this.currentLang);
        });

        return 1;
    }

    static updateLang(lang) {
        for (var k = 0; k < this.textElements.length; k++) {
            var wordKey = this.textArray[k];
            if (lang.id === this.baseLang.id) {
                this.textElements[k].contents().filter(function () {
                    return this.nodeType == 3;
                })[0].nodeValue = " " + wordKey + " "
            } else {
                var phrase = this.translatedText[wordKey];
                if (phrase) {
                    var value = phrase[lang.id];
                    if (value) {
                        this.textElements[k].contents().filter(function () {
                            return this.nodeType == 3;
                        })[0].nodeValue = " " + value + " "

                    }
                }
            }
        }
    }

}
