/**
 * Variables
 * @type {Array}
 */

// Дано
var textArray = []; // Array of a texts
var textElements = []; // All elements with text
var key = document.currentScript.getAttribute('key'); //1

// Найдено
var translatedText = [];
var currentLang = {}; // Selected lang
var allLangs = []; // All available langs
var baseLang = {};

var head = document.getElementsByTagName('head')[0];
var link = document.createElement('link');
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = 'http://webtrans.local/storage/client/translate.css';
link.media = 'all';
//head.appendChild(link);
if (document.createStyleSheet) {
    document.createStyleSheet('http://webtrans.local/storage/client/translate.css');
} else {
    head.append(link);
}


initLibrary();

function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}


/**
 * Main Logic
 */

function initLibrary() {
    //var body = document.getElementsByTagName("body")[0];

    findChild($("body")); // Нашли весь текст
    if (inIframe()) {
        parent.postMessage({data: textArray}, "http://webtrans.local/");
    }
    askServer(); // Запросили у сервера перевод
}

/**
 * FIND ALL TEXT
 *
 * @param elem
 */

function findChild(elem) {
    var cText = elem.clone()    //clone the element
        .children() //select all the children
        .remove()   //remove all the children
        .end()  //again go back to selected element
        .text().replace(/\n/g, '').trim();

    if (cText.length > 0) {
        textArray.push(cText);
        textElements.push(elem);
        // console.log(elem);
        if (inIframe()) {
            elem.mouseover(function (e) {
                elem.addClass("translate-over")
            });
            elem.mouseout(function () {
                elem.removeClass("translate-over")
            });
            elem.click(function () {
                parent.postMessage({index: cText}, "http://webtrans.local/");

                if (elem.prop("tagName") == "BUTTON" || elem.prop("tagName") == "A") {
                    return confirm("Open link?");
                }
                return false;
            })
        }
    }


    if (elem.children().length > 0) {
        elem.children().map(function () {
            findChild($(this))
        })
    }
}

function askServer() {
    $.ajax({
        type: 'POST',
        data: {
            key: key,
            url: window.location.href,
            data: textArray
        },
        url: 'http://webtrans.local/api/translate',
        crossDomain: true,
        success: ajaxSuccess
    });
}


function ajaxSuccess(msg) {
    translatedText = msg.translation; // получили файлик с переводом с долбанули его в массив
    currentLang = msg.baseLanguage; // выбран исходный язык
    baseLang = msg.baseLanguage;
    allLangs = msg.languages;


    var browserLang = navigator.language || navigator.userLanguage;

    browserLang = browserLang.split("-")[0];

    var cookieLang = allLangs.filter(function (e) {
        return e.code === getCookie("current_language");
    })[0];

    if (cookieLang) {
        currentLang = cookieLang;
    } else {
        var blang = allLangs.filter(function (e) {
            return e.code === browserLang
        })[0];
        if (blang) {
            currentLang = blang;
        }
    }
    updateLang(currentLang);
    addSwitchLangBtn(msg.selector);

    if (inIframe()) {
        parent.postMessage({loaded: true}, "http://webtrans.local/");
    }
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

/**
 * ADD A BUTTON IN SPAN
 *
 * @returns {number}
 */

function addSwitchLangBtn(selector) {
    var place = $("#swlgbtn");
    if (place.length === 0) {
        $("body").prepend("<div id='swlgbtn' class='swlgbtn-default'></div>");
        place = $("#swlgbtn");
    }

    if (place.children().length > 0) {
        place.empty()
    }

    // Render block

    var btnOpenCode = "<div id=\"selector-container\">" +
        "  <ul class=\"selector-list\">";
    var btnCloseCode =
        "  </ul>" +
        " <button class=\"selector-btn\" style=''>" +
        currentLang.name +
        "</button>" +
        "</div>";

    var responseCode = btnOpenCode;
    for (var i = 0; i < allLangs.length; i++) {
        responseCode = responseCode + "<a class=\"selector-list-item\" data-lang='" + allLangs[i].id + "' href=\"#\">" + allLangs[i].name + "</a>"
    }
    responseCode = responseCode + btnCloseCode;

    place.append(responseCode);

    // Move it

    /**

     if (inIframe()) {
        var button = document.getElementById('swlgbtn');

        button.onmousedown = function (e) {

            var coords = getCoords(button);
            var shiftX = e.pageX - coords.left;
            var shiftY = e.pageY - coords.top;
            console.log(coords);
            button.style.position = 'absolute';
            //document.body.appendChild(ball);
            //moveAt(e);

            button.style.zIndex = 1000; // над другими элементами

            function moveAt(e) {
                var x = e.clientX, y = e.clientY, elementMouseIsOver = document.elementFromPoint(x, y),
                    children = getChildren(elementMouseIsOver), lastChild = children[children.length - 1];

                console.log(elementMouseIsOver);

                console.log(lastChild);
                //button.style.left = e.pageX - shiftX + 'px';
                //button.style.top = e.pageY - shiftY + 'px';
                //document.removeChild(button);
                elementMouseIsOver.appendChild(button);
                //console.log()
            }

            function getChildren(e) {
                var c = [];
                var tags = ["p", "h5", "h4", "h3", "h2", "h1", "a", "li", "button"];
                for (var tag in tags) {
                    c.concat(e.getElementsByTagName(tag))
                }
                return c;
            }

            document.onmousemove = function(e) {
                var x = e.clientX, y = e.clientY,
                    elementMouseIsOver = document.elementFromPoint(x, y);

                //console.log({left:x,top:y});
                console.log(elementMouseIsOver);

            };

            document.onmouseup = function (e) {
                console.log("UPED");
                moveAt(e);
                document.onmousemove = null;
                button.onmouseup = null;
            };

        };

        button.ondragstart = function () {
            return false;
        };
    }

     **/

        // add styles

    var selectorStyleObj = JSON.parse(selector);
    // кнопка
    for (var btnKey in selectorStyleObj["btn"]) {
        if (btnKey === "hover") {
            $(".selector-btn").hover(function () {
                $(this).css({"background": selectorStyleObj["btn"]["hover"]});
            }, function () {
                $(this).css({"background": selectorStyleObj["btn"]["background"]})
            });
        } else {
            var cssBtn = {};
            if (btnKey === 'font-size' || btnKey === 'padding-left' || btnKey === 'padding-top' || btnKey === 'padding-right' || btnKey === 'padding-bottom') {
                cssBtn[btnKey] = selectorStyleObj["btn"][btnKey] + "px";
            } else {
                cssBtn[btnKey] = selectorStyleObj["btn"][btnKey];
            }
            $(".selector-btn").css(cssBtn);
        }
    }
    // список
    for (var listKey in selectorStyleObj["list"]) {
        if (listKey === "hover") {
            $(".selector-list").hover(function () {
                $(this).css({"background": selectorStyleObj["list"]["hover"]});
            }, function () {
                $(this).css({"background": selectorStyleObj["list"]["background"]})
            });
        } else {
            var cssList = {};
            if (listKey === 'font-size' || listKey === 'padding-left' || listKey === 'padding-top' || listKey === 'padding-right' || listKey === 'padding-bottom') {
                cssList[listKey] = selectorStyleObj["list"][listKey] + "px";
            } else {
                cssList[listKey] = selectorStyleObj["list"][listKey];
            }
            $(".selector-list").css(cssList);
        }
    }
    // ссылка
    for (var listItemKey in selectorStyleObj["list-item"]) {
        if (listItemKey === "hover") {
            $(".selector-list-item").hover(function () {
                $(this).css({"background": selectorStyleObj["list-item"]["hover"]});
            }, function () {
                $(this).css({"background": selectorStyleObj["list-item"]["background"]})
            });
        } else {
            var cssListItem = {};
            if (listItemKey === 'font-size' || listItemKey === 'padding-left' || listItemKey === 'padding-top' || listItemKey === 'padding-right' || listItemKey === 'padding-bottom') {
                cssListItem[listItemKey] = selectorStyleObj["list-item"][listItemKey] + "px";
            } else {
                cssListItem[listItemKey] = selectorStyleObj["list-item"][listItemKey];
            }
            $(".selector-list-item").css(cssListItem);
        }
    }

    // add events

    var selectorList = $(".selector-list");
    $('.selector-btn').click(function () {
        if (selectorList.css("display") === 'block') {
            $(".selector-list").css({"display": "none"})

        } else {
            $(".selector-list").css({"display": "block"})
        }
    });


    $(".selector-list-item").click(function () {
        var needLangId = $(this).data("lang");

        currentLang = allLangs.filter(function (e, i) {

            return e.id === needLangId
        })[0];

        document.cookie = "current_language=" + currentLang.code;

        addSwitchLangBtn(selector);
        updateLang(currentLang);
    });

    return 1;
}


function updateLang(lang) {
    for (var k = 0; k < textElements.length; k++) {
        var wordKey = textArray[k];
        if (lang.id === baseLang.id) {
            textElements[k].contents().filter(function () {
                return this.nodeType == 3;
            })[0].nodeValue = " " + wordKey + " "
        } else {
            var phrase = translatedText[wordKey];
            if (phrase) {
                var value = phrase[lang.id];
                if (value) {
                    textElements[k].contents().filter(function () {
                        return this.nodeType == 3;
                    })[0].nodeValue = " " + value + " "

                }
            }
        }
    }
}

function getCoords(elem) {   // кроме IE8-
    var box = elem.getBoundingClientRect();
    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };
}
