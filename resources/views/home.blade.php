@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include("statuses.success")
                @include("statuses.error")
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("/admin/")}}">ADMIN</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Home</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">
                        Sites
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                @if(count($sites)>0)
                                    <thead>
                                    <tr>
                                        <th>
                                            Base language
                                        </th>
                                        <th>
                                            Domain
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                @endif
                                <tbody>
                                @foreach($sites as $site)
                                    <tr>
                                        <td>
                                            {{$site->language->name}}
                                        </td>
                                        <td>
                                            <a href="{{url("/sites/$site->id")}}">
                                                {{$site->domain}}
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{url("/sites/$site->id")}}" method="post">
                                                @csrf
                                                @method("DELETE")
                                                <button type="submit" class="btn btn-outline-danger btn-sm">
                                                    <i class="material-icons">
                                                        delete
                                                    </i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3">
                                        In order to add a new site, select a base language of your site and write a
                                        domain name
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="3">
                                        <form method="post" action="{{url("/sites")}}">
                                            @csrf
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <select class="form-control" name="language_id">
                                                        @foreach($languages as $language)
                                                            <option value="{{$language->id}}">
                                                                {{$language->name}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <input name="domain" type="text" class="form-control" placeholder="domain.com" aria-label="Recipient's username" aria-describedby="button-addon2">
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Create</button>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        Words count: {{$count}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
