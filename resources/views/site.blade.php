@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                @include("statuses.success")
                @include("statuses.error")
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("/home")}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$site->domain}}</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">Pages</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                @if(count($site->pages)>0)
                                    <thead>
                                    <tr>
                                        <th>
                                            Page path
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                @endif
                                <tbody>
                                @foreach($site->pages as $page)
                                    <tr>
                                        <td>
                                            <a href="{{url("/pages/$page->id")}}"
                                               class="@if($page->dynamic) js-need-vars @endif"
                                               data-vars="{{$page->variables}}">
                                                {{$site->domain}}{{$page->url}} <i>(open and translate)</i>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{url("/pages/$page->id")}}" method="post">
                                                @csrf
                                                @method("DELETE")
                                                <button type="submit" class="btn btn-outline-danger btn-sm">
                                                    <i class="material-icons">
                                                        delete
                                                    </i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2">
                                        In order to add a new page, choose translated languages and then, write a path
                                        below. If you want to add a main page,
                                        just write <var>/</var>
                                    </td>
                                </tr>
                                @if($site->languages->count()>0)
                                    <tr>
                                        <td colspan="2">
                                            <form method="post" action="{{url("/pages")}}">
                                                @csrf
                                                <input type="hidden" name="site_id" value="{{$site->id}}">
                                                <div class="input-group mb-3">
                                                    <input name="url" type="text" class="form-control" placeholder="/"
                                                           aria-label="Recipient's username"
                                                           aria-describedby="button-addon2">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="submit"
                                                                id="button-addon2">Create
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Language selector
                    </div>
                    <div class="card-body">
                        <div id="selector-container">
                            <ul class="selector-list">
                                @foreach($site->languages as $l)
                                    <li class="selector-list-item">
                                        <a>

                                            {{$l->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <button class="selector-btn">{{$site->language->name}}</button>

                        </div>
                        @push("scripts")
                            <script>
                                    @php
                                        $px=["font-size","padding-left","padding-top",'padding-right',"padding-bottom","width","height","margin-left","margin-top",'margin-right',"margin-bottom","border-top-left-radius","border-top-right-radius",'border-bottom-left-radius',"border-bottom-right-radius","border-radius"];
                                        $pc = [];
                                    @endphp
                                    @foreach(json_decode($site->selector,true) as $key=>$value)
                                var object = $(".selector-{{$key}}");
                                @foreach($value as $k=>$v)
                                @if($k=="hover")
                                object.hover(function () {
                                    $(this).css({"background": "{{$v}}"})
                                }, function () {
                                    $(this).css({"background": "{{$value['background']}}"})
                                });
                                    @else
                                var css = {};
                                @if (in_array($k,$px))
                                    css['{{$k}}'] = '{{$v}}' + "px";
                                @else
                                    css['{{$k}}'] = "{{$v}}";

                                @endif
                                object.css(css);
                                @endif
                                @endforeach
                                @endforeach
                            </script>
                        @endpush
                    </div>
                    <div class="card-footer">
                        <a href="{{url("/sites/$site->id/selector")}}">Edit</a>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header">Translated languages</div>

                    <div class="card-body">
                        @if($site->languages->count()>0)
                            {{$site->languages->pluck("name")->implode(",")}}
                        @else
                            <form action="{{url("/sites/$site->id/synclanguages")}}" method="post">
                                @csrf
                                <div class="row">
                                    @foreach($languages as $language)
                                        <div class="form-check col-6">
                                            <input @if($siteLanguages->contains($language->id)) checked @endif
                                            name="languages[]"
                                                   class="form-check-input" type="checkbox"
                                                   value="{{$language->id}}"
                                                   id="lang_{{$language->id}}">
                                            <label class="form-check-label" for="lang_{{$language->id}}">
                                                @if($language->flag)
                                                    <img style="width: 20px" src="{{$language->flag}}">
                                                @endif
                                                {{$language->name}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row mt-3 text-center">
                                    <div class="col">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-outline-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
