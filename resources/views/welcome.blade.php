<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Cloud Translator') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top mb-2">
    <a class="navbar-brand" href="#">
        Cloud
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#features">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#install">Install</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#usage">Usage</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#integration">Integrations</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pricing">Pricing</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="#faq">F.A.Q.</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">GitHub</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} {{Auth::user()->surname}} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{url("/home")}}">Home</a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</nav>

<main role="main" class="container mt-5">
    <div class="row">
        <div class="col">
            <h1>Cloud translator</h1>
            <h2>The easiest way to make your web-page multilingual</h2>
        </div>
    </div>
</main><!-- /.container -->
<section class="container" id="features">
    <div class="row">
        <div class="col">
            <h1>Features</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3>93 languages</h3>
            <p>
                You can choose from as many languages as you like from 93 available. The translation is based on machine
                learning and is suitable for any type of site.
            </p>
        </div>
        <div class="col" id="app">
            <langs-list :langs="'{{$languages}}'"></langs-list>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6 text-center">
            <img src="/images/translator.jpg" style="max-width: 100%">
        </div>
        <div class="col-md-6">
            <h3>Custom translation</h3>
            <p>
                If the translation of a phrase does not satisfy you, we provide the opportunity to clarify the
                translation of phrases. It is easy, convenient, fast.
            </p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <h3>Definition of user language</h3>
            <p>
                After installing the widget, your site will be able to determine the native language of your user and
                select the appropriate one available on the site.
            </p>
        </div>
        <div class="col-md-6 text-center">
            <img src="/images/523.jpg" style="max-width: 245px;width: 100%;">
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6 text-center">
            <img src="/images/524.jpg" style="max-width: 245px;width: 100%;">
        </div>
        <div class="col-md-6">
            <h3>Easy integration</h3>
            <p>To install the widget, you do not need additional knowledge in the development. The installation is to
                copy and paste a few lines into the code of your site. Nothing more.</p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <h3>Fast work</h3>
            <p>
                Our widget weighs so little that you won't notice the difference in page load speed. The weight of the
                translation will depend on the number of phrases on the page and the number of languages. But the widget
                loads them asynchronously, so that users will not see the difference in the speed of opening the window.
            </p>
        </div>
        <div class="col-md-6 text-center">
            <img src="/images/522.jpg" style="max-width: 245px;width: 100%;">
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6 text-center">
            <img src="/images/vars.jpg" style="width: 100%;">
        </div>
        <div class="col-md-6">
            <h3>Translation of pages with variables in URL and dynamic content</h3>
            <p>If you have a page with a dynamic address, for example <var>/user/<i>{username}</i></var> or <var>/articles/<i>{article
                        name}</i></var>,
                then when creating the page, specify the place for the "variable" and our service will automatically
                determine the translation after creating the page and save it.</p>
        </div>

    </div>
    <div class="row mt-5">

        <div class="col-md-6">
            <h3>Custom language selector</h3>
            <p>
                You can customize the appearance of the language selection on the site. Thus, the selection button will
                look like native on your website.
            </p>
        </div>
        <div class="col-md-6 text-center">
            <img src="/images/selector.jpg" style="width: 100%;">
        </div>
    </div>
</section>
<section class="container" id="install">
    <div class="row">
        <div class="col">
            <h1>Install</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h1 class="text-right">1.</h1>
        </div>
        <div class="col">
            <p><a href="{{url("/register")}}" target="_blank">Register</a> in our service. Create a web-site and then a
                web-page.</p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h1 class="text-right">2.</h1>
        </div>
        <div class="col">
            <p>Copy <a href="#integration"> installation lines</a> and paste it in your code of web-page.</p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h1 class="text-right">3.</h1>
        </div>
        <div class="col">
            <p>Open your web-page in our service and click "Translate". After completing the process, your site will be
                ready.</p>
        </div>
    </div>
</section>

<section class="container" id="integration">
    <div class="row">
        <div class="col">
            <h1>Integration</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h2>Step 1</h2>
            <h4>Add some lines in your html code:</h4>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p>
                <button class="btn btn-link js-toggle-btn" type="button" data-url="any">
                    Open
                </button>

            </p>
            <div class="collapse" id="any">
                <div class="card card-body">
                    <p>Between &lt;head&gt; and &lt;/head&gt; </p>
                    <div class="alert alert-secondary" role="alert">
                        &lt;link href="{{env("APP_URL")}}/storage/client/translate.css" rel="stylesheet"
                        type="text/css"/&gt;
                    </div>
                    <p>Where you want to see a selector</p>
                    <div class="alert alert-secondary" role="alert">
                        &lt;span id="swlgbtn"&gt;&lt;/span&gt;
                    </div>
                    <p>Before &lt;/body&gt; </p>
                    <div class="alert alert-secondary" role="alert">
                        &lt;script src="{{env("APP_URL")}}/storage/client/translate.js" key="YOUR_KEY"&gt;&lt;/script&gt;
                    </div>
                    <p><strong>Example:</strong></p>
                    <img src="/images/sample.png" style="max-width: 100%">
                </div>
            </div>
            {{--<div class="collapse" id="wix">
                <div class="card card-body">
                    <p>Between &lt;head&gt; and &lt;/head&gt; </p>
                    <div class="alert alert-secondary" role="alert">
                        &lt;link href="{{env("APP_URL")}}/storage/client/translate.css" rel="stylesheet"
                        type="text/css"/&gt;
                    </div>
                    <p>Where you want to see a selector</p>
                    <div class="alert alert-secondary" role="alert">
                        &lt;span id="swlgbtn"&gt;&lt;/span&gt;
                    </div>
                    <p>Before &lt;/body&gt; </p>
                    <div class="alert alert-secondary" role="alert">
                        &lt;script src="{{env("APP_URL")}}/storage/client/translate.js" key="YOUR_KEY"&gt;&lt;/script&gt;
                    </div>
                </div>
            </div>--}}
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h2>Step 2</h2>
            <h3>Create a web-page in your <a href="{{url("/home")}}">account</a></h3>
        </div>
        <div class="col-12">
            <ul class="list-unstyled">
                <li class="media row">
                    <div class="media-body col-12 col-lg-4">
                        <h4 class="mt-0 mb-1">Create a domain</h4>
                        Here your should type only base domain of your site
                    </div>
                    <img src="/images/create_site.png" class="mr-3 col-12 col-lg-8">
                </li>
                <li class="media row my-4">
                    <div class="media-body col-12 col-lg-4">
                        <h4 class="mt-0 mb-1">Choose languages</h4>
                        Choose languages you want to translate your site to. Do not forger to save them
                    </div>
                    <img src="/images/choose_lang.png" class="mr-3 col-12 col-lg-8">
                </li>
                <li class="media row">
                    <div class="media-body col-12 col-lg-4">
                        <h4 class="mt-0 mb-1">Create a page</h4>
                        Create a page your want to translate. Just type a path, without a domain name. For <i>index</i> page type <i>/</i>
                    </div>
                    <img src="/images/create_page.png" class="mr-3 col-12 col-lg-8">
                </li>
                <li class="media row">
                    <div class="media-body col-12 col-lg-4">
                        <h4 class="mt-0 mb-1">Ready!</h4>
                       Wait for our auto translation and then customize text if you need. First time loading may spend some time (1-3 min)
                    </div>
                    <img src="/images/waiting_translation.png" class="mr-3 col-12 col-lg-8">
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="container" id="usage">
    <div class="row">
        <div class="col">
            <h1>Usage</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3>Change supported languages</h3>
            <p>In order to add or remove a language on the site, simply select the list of desired languages in your
                account</p>
        </div>
        <div class="col">
            <img src="/images/lang_chooser.jpg" style="max-width: 245px;width: 100%;">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3>Customize</h3>
            <p>
                Change the translation if necessary
            </p>
        </div>
        <div class="col">
            <img src="/images/translator.jpg" style="max-width: 100%">
        </div>
    </div>
</section>
<section class="container" id="pricing">
    <div class="row">
        <div class="col">
            <h1 class="mb-0">Pricing</h1>
        </div>
    </div>
    <div class="row">
        <div class="col mt-5 price-item">
            <h3>Free</h3>
            <p>
                For small sites and testing of our features
            </p>
            <ul>
                <li>1 translated language</li>
                <li>3 000 words</li>
            </ul>
            <a class="btn btn-outline-primary" href="{{url("/home")}}">Free</a>
        </div>
        <div class="col mt-5 price-item">
            <h3>Base</h3>
            <p>
                For small sites and testing of our features
            </p>
            <ul>
                <li>1 translated language</li>
                <li>10 000 words</li>
            </ul>
            <a class="btn btn-outline-primary" href="{{url("/home")}}">&euro; 8 / month</a>
        </div>
        <div class="col mt-5 price-item">
            <h3>Business</h3>
            <p>
                Best for landings
            </p>
            <ul>
                <li>3 translated languages</li>
                <li>50 000 words</li>
            </ul>
            <a class="btn btn-outline-primary" href="{{url("/home")}}">&euro; 15 / month</a>
        </div>
        <div class="col mt-5 price-item">
            <h3>Professional</h3>
            <p>
                Best for multipages sites
            </p>
            <ul>
                <li>93 languages</li>
                <li>200 000 words</li>
                <li>Variables in URL <i>(for fast working)</i></li>
            </ul>
            <a class="btn btn-outline-primary" href="{{url("/home")}}">&euro; 40 / month</a>
        </div>
        <div class="col mt-5 price-item">
            <h3>Unlimited</h3>
            <p>
                Best for corporate sites
            </p>
            <ul>
                <li>93 languages</li>
                <li>Unlimited words</li>
                <li>Variables in URL <i>(for fast working)</i></li>
            </ul>
            <a class="btn btn-outline-primary" href="{{url("/home")}}">&euro; 200 / month</a>
        </div>
    </div>
</section>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script>
    $(".js-toggle-btn").click(function () {

        var block = $('#' + $(this).data("url"));
        var shown = block.css("display") === 'block';
        $('.collapse').collapse("hide");
        if (!shown) {
            block.collapse("show");
        }
    })
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52440946, "init", {
        id:52440946,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52440946" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
