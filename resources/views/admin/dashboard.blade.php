@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include("statuses.success")
                @include("statuses.error")
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">ADMIN</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">
                        Dashboard
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @foreach($users as $user)
                            <div class="card card-primary mt-3">
                                <div class="card-header">
                                    {{$user->name}} {{$user->surname}} @if($user->email_verified_at) <span class="badge badge-success">V</span> @endif
                                </div>
                                <div class="card-body">
                                    <p>Email: <span class="text-primary">{{$user->email}}</span></p>
                                    <p>Registered: {{$user->created_at}}</p>
                                    <p>Sites: {{count($user->sites)}}</p>
                                    @if(count($user->sites)>0)
                                        <ul>
                                            @foreach($user->sites as $site)
                                                <li>
                                                    <p>Domain: <strong class="text-success">{{$site->domain}}</strong></p>
                                                    <p>Base language: {{$site->language->name}}</p>
                                                    <p>Translate
                                                        languages: {{$site->languages->pluck("name")->implode(", ")}}</p>
                                                    <ul>
                                                        @foreach($site->pages as $page)
                                                            <li>
                                                                <p>{{$site->domain}}{{$page->url}} <span
                                                                        class="text-muted">{{count($page->phrases)}} phrases</span>
                                                                </p>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        @endforeach

                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
