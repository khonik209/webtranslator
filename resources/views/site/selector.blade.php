@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("/home")}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url("/sites/$site->id")}}">{{$site->domain}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Selector</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">Selector</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <site-selector :selector="'{{$site->selector}}'" :base_lang="'{{$site->language}}'"
                                       :langs="'{{$site->languages}}'" :site_id="'{{$site->id}}'"></site-selector>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
