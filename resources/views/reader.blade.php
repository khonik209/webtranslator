@extends('layouts.app')

@section('content')
<div class="container-fluid" id="app">
    <div class="row">
       <div class="col">
           <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                   <li class="breadcrumb-item"><a href="{{url("/home")}}">Home</a></li>
                   <li class="breadcrumb-item"><a href="{{url("/sites/$page->site_id")}}">{{$uri}}</a></li>
                   <li class="breadcrumb-item active" aria-current="page">{{$page->url}}</li>
               </ol>
           </nav>
       </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-body">
                    <h4>Easy translating</h4>
                    <p>1. Choose an element you want to translate in left window</p>
                    <p>2. Update the translation in right window</p>
                    <p>3. Click "Save" button</p>
                </div>
            </div>
        </div>
    </div>
    <read-site :uri="'{{$uri}}'" :page_id="'{{$page->id}}'" :base_language_id="'{{$page->site->language_id}}'" :langs="'{{json_encode($languages)}}'"></read-site>
</div>
@endsection
