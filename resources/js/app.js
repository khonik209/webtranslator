
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'

Vue.use(require('vue-resource'));

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

 const files = require.context('./', true, /\.vue$/i)
 files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

$("#searcher").change(function () {

});

const app = new Vue({
    el: '#app'
});

$('.js-need-vars').click(function () {
    var vars = $(this).data("vars");
    var formCode = "<form class='js-vars-filled' action='" + $(this).prop("href") + "' method='GET'></form>";

    $(this).parent("td").append(formCode);

    var form = $(this).parent("td").find("form");

    for (var i = 0; i < vars.length; i++) {
        form.append("<div class='form-group'><input required class='form-control' type='text' name='" + vars[i] + "' placeholder='" + vars[i] + "' ></div>")
    }
    form.append("<div class='form-group'> <button type='submit' class='btn btn-dark btn-sm'>Continue</button></div>");

    $(".js-vars-filled").submit(function () {
        var vars = $(this).serializeArray();
        for (var k = 0; k < vars.length; k++) {
            console.log(vars[k]);
            if (vars[k].value === "") {
                return false;
            }
        }
        return true;
    });

    return false;
});

var selectorList = $(".selector-list");
$('.selector-btn').click(function () {
    if (selectorList.css("display") === 'block') {
        $(".selector-list").css({"display": "none"})

    } else {
        $(".selector-list").css({"display": "block"})
    }
});
