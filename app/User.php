<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',"surname", 'email', 'password', "key"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sites()
    {
    	return $this->hasMany("App\Site");
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format("d.m.Y H:i");
    }

    /**
     * Число используемых слов
     *
     * @return int
     */
    public function wordsCount()
    {
        $sites = $this->sites;
        $texts = PhraseLanguage::whereHas("phrase", function ($q) use ($sites) {
            $q->whereHas("page", function ($query) use ($sites) {
                $query->whereIn("site_id", $sites->pluck("id"));
            });
        })->get()->unique("phrase_id")->pluck("value");

        $count = 0;
        foreach ($texts as $text) {
            $c = count(explode(" ", $text));
            if ($text && $c > 0) {
                $count = $count + $c;
            }
        }
        return $count;
    }


    /**
     * Отличаем Админа
     *
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->id == 1;
    }
}
