<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phrase extends Model
{
	public function page()
	{
		return $this->belongsTo("App\Page");
	}

	public function languages()
	{
		return $this->belongsToMany("App\Language");
	}

	public function phraseLanguages()
	{
		return $this->hasMany("App\PhraseLanguage");
	}


}
