<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Phrase;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TranslateController extends Controller {
	public function index( Request $request ) {
		if ( isset( $_SERVER["HTTP_ORIGIN"] ) === true ) {
			$origin          = $_SERVER["HTTP_ORIGIN"];
			$allowed_origins = array(
				"http://expert.local",
				"http://verifier.local"
			);
			if ( in_array( $origin, $allowed_origins, true ) === true ) {
				header( 'Access-Control-Allow-Origin: ' . $origin );
				header( 'Access-Control-Allow-Credentials: true' );
				header( 'Access-Control-Allow-Methods: POST,GET' );
				header( 'Access-Control-Allow-Headers: Content-Type' );
			}
			if ( $_SERVER["REQUEST_METHOD"] === "OPTIONS" ) {
				exit; // OPTIONS request wants only the policy, we can stop here
			}
		}

		$user = User::where( "key", $request->key )->first();

		if ( ! $user ) {
			return response()->json( [
				"success" => 0
			] );
		}

		if ( 0 ) {
			// todo Сюда всякие другие проверки, по оплате, хуяте и тд.
			return response()->json( [
				"success" => 0
			] );
		}

		$parsedUrl = parse_url( $request->url );


        $site = $user->sites->where("domain", $parsedUrl["host"])->first();

        if (!$site) {
            return response()->json([
                "success" => 0,
            ]);
        }

		$languages = $site->languages->push( $site->language );

		$page = $site->pages->where( "url", $parsedUrl["path"] )->first();

        if (!$page) {
            return response()->json([
                "success" => 0,
                "translation" => [],
                "baseLanguage" => $site->language,
                "languages" => $languages
            ]);
        }

        if (!$page) {

            // Если страница не найдена, то смотрим, мб дело в переменных
            $exploded = explode("/", $request->url);
            // [order,224]
            $page = $site->pages()->where("url", "like", "%$exploded[1]%")->where("dynamic", 1)->first(); // todo здесь НЕ масштабируемо. Сложные адреса все сломают
        }

		$textArray = $request->data;

		$currentPhrases = $page->phrases()->with( "phraseLanguages" )->whereHas( "phraseLanguages", function ( $q ) use ( $site, $textArray ) {
			$q->where( 'language_id', $site->language_id )->whereIn( DB::raw( 'BINARY `value`' ), $textArray );
		} )->get()->pluck( "phraseLanguages" )->map( function ( $v, $k ) use ( $site ) {

			$key = $v->where( 'language_id', $site->language_id )->first();

			$value = $v->where( "language_id", "!=", $site->language_id )->groupBy( "language_id" )->map( function ( $p ) {
				return $p[0]->value;
			} )->toArray();
			return [ $key->value => $value ];
		} )->collapse();

        $wordsCount = $user->wordsCount();
        if ($wordsCount < 5000) {
            $keys = $currentPhrases->keys()->all();

            $diff = array_diff($textArray, $keys);
            if (count($diff) > 0) {
                foreach ($diff as $str) {
                    if (is_numeric($str)) {
                        continue;
                    }
                    $transArray = [];
                    $sync[$site->language_id] = ["value" => $str];
                    foreach ($site->languages as $language) {
                        $response = $this->translate($str, $site->language->code, $language->code); // не только переводить, но и сохранять! Нахуя нам каждый раз при обновлении страницы яндекс дергать?
                        $translatedString = $response->text;
                        $transArray[$language->id] = $translatedString;
                        $sync[$language->id] = ["value" => $translatedString[0]];
                    }
                    $currentPhrases[$str] = $transArray;
                    $phrase = new Phrase;
                    $phrase->page_id = $page->id;
                    $phrase->save();

                    $phrase->languages()->sync($sync);
                    $c = count(explode(" ", $str));
                    if ($c > 0) {
                        $wordsCount = $wordsCount + $c;
                    }
                    if ($wordsCount >= 5000) {
                        break;
                    }
                }
            }
        }
		return response()->json( [
			"success" => 1,
			"translation" => $currentPhrases,
			"baseLanguage" => $site->language,
            "languages" => $languages,
            "selector" => $site->selector
		] );
	}

	protected function translate( $text, $fromLangCode = "ru", $toLangCode = "en" ) {
		$data = [
			"text" => $text,
			'format' => 'plain',
			'lang' => "$fromLangCode-$toLangCode",
			'key' => 'trnsl.1.1.20190109T091943Z.02ba0661ff6c7033.391489c3c4e713773ead25ef30193c9bdcce6173'

		];
		$str  = http_build_query( $data );
		$url  = "https://translate.yandex.net/api/v1.5/tr.json/translate?$str";
		$ch   = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		$returned = curl_exec( $ch );
		curl_close( $ch );
		$response = json_decode( $returned );

		return $response;
	}
}
