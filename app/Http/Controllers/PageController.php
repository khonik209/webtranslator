<?php

namespace App\Http\Controllers;

use App\Page;
use App\Phrase;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
        $site = Site::findOrFail($request->site_id);
        $checkPages = $site->pages->count();
        if ($checkPages > 0) {
        //    return back()->with( "error", "By your subscription, only 1 page is available." );
        }
		$page          = new Page();
		$page->url     = $request->url;
		$page->site_id = $request->site_id;

        preg_match_all("#\{(.*?)\}#", $request->url, $checkVars);
        $vars = $checkVars[0];
        if (count($vars) > 0) {
            return back()->with( "error", "By your subscription, variables in URL are unavailable" );
            $page->dynamic = 1;
            $page->variables = json_encode($vars);
        }

		$page->save();

		return back()->with( "success", "Page has been created" );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Page $page
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function show(Page $page, Request $request)
    {

        foreach ($request->all() as $key => $value) {
            $page->url = str_replace($key, $value, $page->url);
        }
		$uri = "http://" . $page->site->domain . $page->url;

		$baseL = $page->site->language_id;

		$languages = $page->site->languages()->where( "languages.id", "!=", $baseL )->get();
		return view( "reader", compact( 'uri', 'page', 'languages' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Page $page
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( Page $page ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\Page $page
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, Page $page ) {
		//
	}

	/**
	 * @param Page $page
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */

	public function destroy( Page $page ) {
		$page->delete();

		return back()->with( "success", "Page has been deleted" );
	}

	/**
	 * Сохранение итогового перевода
	 *
	 * @param Request $request
	 *
	 * @return int
	 */

	public function save( Request $request ) {
		$page = Page::find( $request->pageId );
		$site = $page->site;

		// Обновили перевод

        foreach ($request->translatedText as $key => $value) {
            // key - фраза на исходном языке
            // value - массив перевода [language_id=>string]

            $ph = $page->phrases()->whereHas("phraseLanguages", function ($q) use ($site, $key) {
                $q->where('language_id', $site->language_id)->where(DB::raw('BINARY `value`'), $key);
            })->first();


            $translation = [
                $site->language_id=>[
                    "value"=>$key
                ]
            ];
            $transStrings = collect($value);
            foreach ($transStrings as $id => $string) {

                $translation[$id] = ["value" => $string];
			}

			$ph->languages()->sync( $translation );
		}
        return response()->json([
				"success" => 1
			] );
	}

    /**
     *  Подргужаем текст в редактор
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

	public function fetch( Request $request ) {
		//  uri , textArray, pageId
		$page      = Page::find( $request->pageId );
		$textArray = $request->textArray;
		//$baseLanguage = $page->site->language;

		$site = $page->site;

		$currentPhrases = $page->phrases()->with( "phraseLanguages" )->whereHas( "phraseLanguages", function ( $q ) use ( $site, $textArray ) {
			$q->where( 'language_id', $site->language_id )->whereIn( DB::raw( 'BINARY `value`' ), $textArray );
		} )->get()->pluck( "phraseLanguages" )->map( function ( $v, $k ) use ( $site ) {

			$key = $v->where( 'language_id', $site->language_id )->first();

			$value = $v->where( "language_id", "!=", $site->language_id )->groupBy( "language_id" )->map( function ( $p ) {
				return $p[0]->value;
			} )->toArray();
			return [ $key->value => $value ];
		} )->collapse();

		return response()->json( [
			"text" => $currentPhrases
		] );
	}
}
