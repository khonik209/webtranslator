<?php

namespace App\Http\Controllers;

use App\User;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::with("sites","sites.pages","sites.pages.phrases","sites.languages","sites.language")->orderBy("created_at","desc")->paginate(15);
        return view("admin.dashboard",compact('users'));
    }
}
