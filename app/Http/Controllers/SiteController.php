<?php

namespace App\Http\Controllers;

use App\Language;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $checkSites = $user->sites->count();
        if ($checkSites > 0) {
            //    return back()->with("error", "By your subscription, only 1 site is available.");
        }

        $selector = [
            "btn" => [
                "color" => "#333",
                "background" => "#fff",
                "font-size" => 11,
                "hover" => "#e0e0e0",
                "padding-left" => 5,
                "padding-right" => 5,
                "padding-top" => 5,
                "padding-bottom" => 5,
                "border-radius" => 30,
                "width" => 60,
                "height" => 60,
                "border" => "1px solid"
            ],
            "list" => [
                "color" => "#333",
                "background" => "rgba(255, 255, 255, 0);",
                "font-size" => null,
                "hover" => "rgba(255, 255, 255, 0);",
                "padding-left" => 0,
                "padding-right" => 0,
                "padding-top" => 0,
                "padding-bottom" => 0,
                "box-shadow" => "none"
            ],
            "list-item" => [
                "color" => "#333",
                "background" => "#fff",
                "font-size" => 11,
                "hover" => "#e0e0e0",
                "padding-left" => 20,
                "padding-right" => 20,
                "padding-top" => 3,
                "padding-bottom" => 3,
                "margin-left" => 0,
                "margin-right" => 0,
                "margin-top" => 10,
                "margin-bottom" => 10,
                "border-top-left-radius" => 9,
                "border-top-right-radius" => 0,
                "border-bottom-left-radius" => 0,
                "border-bottom-right-radius" => 9,
                "box-shadow" => "1px 1px 1px #00000047"
            ],
            "settings" => [
                "flag" => "1",
                "name" => "1"
            ]
        ];

        //"{\"btn\":{\"color\":\"#333\", \"background\":\"#fff\",\"font-size\":14,\"hover\":\"#e0e0e0\",\"padding-left\":25,\"padding-right\":25,\"padding-top\":5,\"padding-bottom\":5},\"list\":{\"color\":\"#333\",\"background\":\"#fff\",\"font-size\":null,\"hover\":\"#fff\",\"padding-left\":0,\"padding-right\":0,\"padding-top\":5,\"padding-bottom\":5},\"list-item\":{\"color\":\"#333\",\"background\":\"#fff\",\"font-size\":14,\"hover\":\"#e0e0e0\",\"padding-left\":20,\"padding-right\":20,\"padding-top\":3,\"padding-bottom\":3}}"

        $site = new Site();
        $site->domain = $request->domain;
        $site->user_id = $user->id;
        $site->language_id = $request->language_id;
        //    padding: 0;
        //    width: 60px;
        //    height: 60px;
        $site->selector = json_encode($selector);
        $site->save();

        return back()->with("success", "Site has been created");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Site $site
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        $languages = Language::all();
        $siteLanguages = $site->languages->pluck("id");
        return view("site", compact('site', 'languages', 'siteLanguages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Site $site
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Site $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        //
    }

    /**
     * @param Site $site
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Site $site)
    {
        $site->delete();
        return back()->with("success", "Site has been deleted");
    }

    /**
     * Sync List of site languages
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function syncLanguages($id, Request $request)
    {
        if (count($request->languages) > 1) {

            //    return back()->with("error","By your subscription, only 1 language is available.");
        }
        $site = Site::findOrFail($id);
        $langs = $request->languages;
        $site->languages()->sync($langs);
        return back()->with("success", "Languages has been updated");
    }

    public function selectorPage($id)
    {
        $site = Site::findOrFail($id);
        return view("site.selector", compact('site'));
    }

    public function selector($id, Request $request)
    {
        $site = Site::findOrFail($id);
        $site->selector = json_encode($request->all());
        $site->save();

        return response()->json([
            'success' => 1
        ]);
    }
}
