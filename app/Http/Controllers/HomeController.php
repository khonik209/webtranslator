<?php

namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index() {
		$user      = Auth::user();
		$sites     = $user->sites;
		$languages = Language::all();

        $texts = \App\PhraseLanguage::whereHas("phrase", function ($q) use ($sites) {
            $q->whereHas("page", function ($query) use ($sites) {
                $query->whereIn("site_id", $sites->pluck("id"));
            });
        })->get()->unique("phrase_id")->pluck("value");

        $count = 0;
        foreach ($texts as $text) {
            $c = count(explode(" ", $text));
            if ($text && $c > 0) {
                $count = $count + $c;
            }
        }

		return view( 'home', compact( 'sites', 'languages','count' ) );
	}
}
