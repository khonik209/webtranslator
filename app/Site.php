<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model {

	public function user() {
		return $this->belongsTo( "App\User" );
	}

	public function pages() {
		return $this->hasMany( "App\Page" );
	}

    public function language()
    {
		return $this->belongsTo( "App\Language" );
	}

    public function languages()
    {
        return $this->belongsToMany("App\Language");
    }
}
