<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhraseLanguage extends Model {

	protected $table = "language_phrase";

	public function phrase() {
		return $this->belongsTo( "App\Phrase" );
	}

	public function language() {
		return $this->belongsTo( "App\Language" );
	}
}
