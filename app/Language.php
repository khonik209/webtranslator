<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Language extends Model
{
    public function phrases()
    {
        return $this->belongsToMany("App\Phrase");
    }

    /*public function sites()
    {
    	return $this->hasMany("App\Site");
    }*/

    public function phraseLanguages()
    {
        return $this->hasMany("App\PhraseLanguage");
    }

    public function sites()
    {
        return $this->belongsToMany("App\Site");
    }

    public function getFlagAttribute($value)
    {
        if (!$value) {
            return null;
        }
        $url = Storage::disk("public")->url($value);
        return $url;
    }

}
