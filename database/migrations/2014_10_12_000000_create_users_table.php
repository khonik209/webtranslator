<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'users', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'name' )->nullable();
			$table->string( 'surname' )->nullable();
			$table->string( 'email' )->unique();
			$table->timestamp( 'email_verified_at' )->nullable();
			$table->string( 'password' );
			$table->string( 'key' )->nullable();
			$table->rememberToken();
			$table->timestamps();
		} );
		Schema::create( 'languages', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( "name" );
			$table->string( "code" );
			$table->timestamps();
		} );
		Schema::create( 'sites', function ( Blueprint $table ) {
			$table->increments( 'id' );
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('language_id')->unsigned()->nullable();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
			$table->string( "domain" )->nullable();
			$table->timestamps();
		} );
		Schema::create( 'pages', function ( Blueprint $table ) {
			$table->increments( 'id' );
            $table->integer('site_id')->unsigned()->nullable();
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
			$table->string( "url" )->nullable();
			$table->string( "translate" )->nullable();
			$table->timestamps();
		} );

		Schema::create( 'phrases', function ( Blueprint $table ) {
			$table->increments( 'id' );
            $table->integer('page_id')->unsigned()->nullable();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
			$table->integer( "order" )->nullable();
			$table->boolean( "primary" )->default( 0 );
			$table->timestamps();
		} );
		Schema::create( 'language_phrase', function ( Blueprint $table ) {
			$table->increments( 'id' );
            $table->integer('language_id')->unsigned()->nullable();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
            $table->integer('phrase_id')->unsigned()->nullable();
            $table->foreign('phrase_id')->references('id')->on('phrases')->onDelete('cascade');
			$table->longText( "value" )->nullable();
			$table->timestamps();
		} );

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'language_phrase' );
		Schema::dropIfExists( 'phrases' );
		Schema::dropIfExists( 'pages' );
		Schema::dropIfExists( 'sites' );
		Schema::dropIfExists( 'languages' );
		Schema::dropIfExists( 'users' );
	}
}
