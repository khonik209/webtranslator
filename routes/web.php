<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Route::get('/', function () {
    $languages = \App\Language::all();
    return view('welcome',compact('languages'));
});

Route::resource("users","UserController");
Route::resource("pages","PageController")->middleware('verified');;
Route::resource("phrases","PhraseController");
Route::resource("sites","SiteController");

Auth::routes();

//Route::get("/read","SiteController@read");
Route::post("/render","PageController@render");
Route::post("/updatephrases","PageController@fetch");
Route::post("/save","PageController@save");


Route::get('/home', 'HomeController@index')->name('home');
Route::post("/sites/{id}/synclanguages","SiteController@syncLanguages");
Route::get("/sites/{id}/selector","SiteController@selectorPage");
Route::post("/sites/{id}/selector","SiteController@selector");

Route::prefix('admin')->group(function () {
    Route::get('/', "AdminController@index");
});
