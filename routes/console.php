<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');
Artisan::command('add_lang', function () {

    $data = [
        "ui" => "en",
        'key' => 'trnsl.1.1.20190109T091943Z.02ba0661ff6c7033.391489c3c4e713773ead25ef30193c9bdcce6173'

    ];
    $str = http_build_query($data);
    $url = "https://translate.yandex.net/api/v1.5/tr.json/getLangs?$str";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $returned = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($returned);
    $langs = $response->langs;
    foreach ($langs as $k => $v) {
        $l = new \App\Language;
        $l->name = $v;
        $l->code = $k;
        $l->save();
    }
});
Artisan::command('calc_words', function () {
    $user = \App\User::find(3);
    $sites = $user->sites;

    $texts = \App\PhraseLanguage::whereHas("phrase", function ($q) use ($sites) {
        $q->whereHas("page", function ($query) use ($sites) {
            $query->whereIn("site_id", $sites->pluck("id"));
        });
    })->get()->unique("phrase_id")->pluck("value");

    $count = 0;
    foreach ($texts as $text) {
        print $text . "\n";
        $c = count(explode(" ", $text));
        if ($text && $c > 0) {
            $count = $count + $c;
        }
    }
    echo $count . "\n";
});

Artisan::command('save_flags', function () {
    $langs = \App\Language::all();
    foreach ($langs as $l) {
        try {
            $path = "https://www.countryflags.io/$l->code/flat/64.png";
            $data = file_get_contents($path);
            $filename = "flags/" . $l->code . ".png";
            \Illuminate\Support\Facades\Storage::disk("public")->put($filename, $data);
            $l->flag = $filename;
            $l->save();
        } catch (\Exception $e) {
            continue;
        }
    }
});

Artisan::command('update_flags', function () {

    $langs = [
        "Afrikaans" => "za", // ЮАР
        "Amharic" => "et", //Эфиопия
        "Arabic" => "", // Арабский
        "Bashkir" => "DELETE", // Башкирский
        "Belarusian" => "by", // Белорусский
        "Bengali" => "DELETE", // Бенгальский
        "Bosnian" => "ba", // Боснийский
        "Catalan" => "", // Каталонский
        "Cebuano" => "ph", // Филиппинский
        "Czech" => "cz", // Чешский
        "Welsh" => "", // Валийский
        "Danish" => "dk", // Датский
        "Greek" => "gr", // Греческий
        "English" => "gb", // Английский
        "Esperanto" => "DELETE", // Удалить нахуй
        "Estonian" => 'ee', // Эстонский
        "Basque" => "DELETE", // Удалить нахуй
        "Persian" => "", // Персидский
        "Irish" => "ie", // Ирландский
        "Scottish Gaelic" => "", // Шотландский
        "Galician" => "DELETE", // Удалить нахуй
        "Gujarati" => "DELETE", // Удалить? Один из языков индии
        "Hebrew" => "il", // Иврит
        "Hindi" => "in", // Хинди (Индия)
        "Armenian" => "am", // Армянский
        "Japanese" => "jp", // Японский
        "Javanese" => "DELETE", // Удалить нахуй
        "Georgian" => "ge", // Грузинский
        "Kazakh" => "kz", // Казахский
        "Khmer" => "kh", // Кхмерский, Камбоджа
        "Kannada" => "DELETE", // Удалить (опять, индия)
        "Korean" => "kr", // Корейский
        "Kyrgyz" => "kg", // Киргизский
        "Latin" => "DELETE", // Латинский - удалить
        "Luxembourgish" => "lu", // Люксмембурский
        "Lao" => "la", // Лаос (флаг отобрать у латинского)
        "Malagasy" => "DELETE", // Мадагаскарский, удалить
        "Mari" => "DELETE", // Удалить
        "Maori" => "DELETE",// Удалить
        "Malayalam" => "DELETE", // Удалить
        "Marathi" => "DELETE", // Удалить
        "Hill Mari" => "DELETE", // Удалить
        "Malay" => "DELETE", // Удалить
        "Burmese" => "DELETE", // Удалить
        "Nepali" => "DELETE", // Удалить
        "Dutch" => "nl", // Голландский
        "Punjabi" => "DELETE", // Удалить
        "Papiamento" => "DELETE", // Удалить
        "Sinhalese" => "DELETE", // Удалить
        "Slovenian" => "si", // Словенский
        "Albanian" => "al", // Албанский
        "Serbian" => "rs", // Сербский
        "Sundanese" => "DELETE", // Удалить
        "Swedish" => "se", // Шведский
        "Swahili" => "DELETE", // Удалить
        "Tamil" => "DELETE", // Удалить
        "Telugu" => "DELETE", // Удалить
        "Tajik" => "DELETE", // Удалить
        "Tagalog" => "DELETE", // Удалить
        "Tatar" => "DELETE", // Удалить
        "Udmurt" => "DELETE", // Удалить
        "Ukrainian" => "ua", // Украинский
        "Urdu" => "DELETE", // Удалить
        "Vietnamese" => "vn", // Вьетнамский
        "Xhosa" => "DELETE", // Удалить
        "Yiddish" => "DELETE",// Удалить
        "Chinese" => "cn", // Китайский
    ];

    foreach ($langs as $name => $code) {
        if (!$code) {
            echo $name . "\n";
            continue;
        }

        continue;

        $find = \App\Language::where("name", $name)->first();
        if (!$find) {
            continue;
        }

        if ($code === "DELETE") {
            $find->delete();
            continue;
        }

        try {
            $path = "https://www.countryflags.io/$code/flat/64.png";
            $data = file_get_contents($path);
            $filename = "flags/" . $code . ".png";
            \Illuminate\Support\Facades\Storage::disk("public")->put($filename, $data);
            $find->flag = $filename;
            $find->save();
        } catch (\Exception $e) {
            continue;
        }
    }
});

Artisan::command('delete_flags', function () {
    $a = [
        'Arabic',
        'Catalan',
        'Welsh',
        'Persian',
        'Scottish Gaelic',
    ];
    \App\Language::whereIn("name", $a)->update(["flag" => null]);
});
